import Course from "../components/Course"

// Boostrap Components
import Container from 'react-bootstrap/Container'

// Data Imports
import courses from '../mock-data/courses'

const Courses = () =>{
    const CourseCards = courses.map((course)=>{
        return(
            <Course course={course} key={course.id} />
        )
    })

    return(
        <Container fluid>
            {CourseCards}
        </Container>
    )
}

export default Courses