import { useState, useEffect } from "react"
import Form from "react-bootstrap/esm/Form"
import Container from "react-bootstrap/esm/Container"
import Button from "react-bootstrap/esm/Button"

const Login = () =>{
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

useEffect(()=>{
    let isEmailNotEmpty = email !== '';
    let isPasswordNotEmpty = password !== '';

    if(isEmailNotEmpty && isPasswordNotEmpty == true){
        setIsDisabled(false)
    }else{
        setIsDisabled(true)
    }
},[email, password])

const register = (e) =>{
    e.preventDefault()
    alert('You are now logged in')
}
    return(
        <Container fluid>
            <Form onSubmit={register}>
                <Form.Group>
                    <h1>Login</h1>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=> setEmail(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={password} onChange={(e)=>setPassword(e.target.value)}></Form.Control>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}

export default Login