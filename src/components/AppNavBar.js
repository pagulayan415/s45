// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const AppNavBar = () =>{
    return(
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>

    )
}

export default AppNavBar