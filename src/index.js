import React from 'react';
import ReactDOM from 'react-dom';
import AppNavBar from './components/AppNavBar';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Page Components
import Home from './pages/Home';
import Courses from './pages/courses';
import Register from './pages/Register';
import Login from './pages/Login';

ReactDOM.render(
  <React.StrictMode>
    <AppNavBar />
    <Login />
    {/* <Register />
    <Home />
    <Courses /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

